import org.server.ChatServer;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            ChatServer server = new ChatServer(1234, 10);
            server.listen();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
