package org.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ChatServer {
    private final int port;
    private final ExecutorService connectionPool;

    public ChatServer(int port,  int poolSize) throws IOException {
        this.port = port;
        connectionPool = Executors.newFixedThreadPool(poolSize);
    }

    public void listen() throws IOException {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
        while (!serverSocket.isClosed()) {
            Socket connection = serverSocket.accept();
            System.out.println("New user connected!");
            UserConnection userConnection = new UserConnection(connection);
            connectionPool.submit(userConnection);
        }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
    }

}
