package org.server;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class UserConnection implements Runnable {
    public static ArrayList<UserConnection> clientConnections = new ArrayList<>();
    public Socket socket;
    private BufferedReader bReader;
    private BufferedWriter bWriter;
    private String nickName;

    public UserConnection(Socket socket) {
        try {
            this.socket = socket;
            this.bWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.bReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.nickName = bReader.readLine();
            clientConnections.add(this);
            sendMsgToAll("SERVER: " + "[" + nickName + "]" + " has entered in the room");
        } catch (IOException e) {
            closeAll(socket, bReader, bWriter);
        }
    }

    @Override
    public void run() {
        String msgFromClient;
        while (socket.isConnected()) {
            try {
                msgFromClient = bReader.readLine();
                sendMsgToAll(msgFromClient);
            } catch (IOException e) {
                closeAll(socket, bReader, bWriter);
                break;
            }
        }
    }

    public void sendMsgToAll(String messageToSend) {
        for (UserConnection connections : clientConnections) {
            try {
                if (!connections.nickName.equals(nickName)) {
                    connections.bWriter.write(messageToSend);
                    connections.bWriter.newLine();
                    connections.bWriter.flush();
                }
            } catch (IOException e) {
                closeAll(socket, bReader, bWriter);
            }
        }
    }

    public void removeClientConnection() {
        clientConnections.remove(this);
        sendMsgToAll(nickName + " has left the server");
    }

    public void closeAll(Socket socket, BufferedReader buffReader, BufferedWriter buffWriter) {
        removeClientConnection();
        try {
            if (buffReader != null) {
                buffReader.close();
            }
            if (buffWriter != null) {
                buffWriter.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.getStackTrace();
        }
    }
}
